﻿using oTees.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace oTees.DAL
{
    public class OteesInitializer : DropCreateDatabaseAlways<OteesDBContext>
    {
        protected override void Seed(OteesDBContext context)
        {
            var roles = new List<Role>
            {
                new Role { Name = "User" },
                new Role { Name = "Admin" },
            };
            roles.ForEach(s => context.Roles.Add(s));
            context.SaveChanges();

            var users = new List<User>
            {
                new User { Username = "3334", FirstName = "test", LastName = "muster", Email = "test@musterseite.at", Password = "xxx", BirthDate = DateTime.Parse("2005-09-01"), CreationDate = DateTime.Parse("2005-09-01"), RoleID = 2},
                new User { Username = "hans", FirstName = "hans", LastName = "peter", Email = "hans@peter.at", Password = "xxx", BirthDate = DateTime.Parse("2005-09-01"), CreationDate = DateTime.Parse("2005-09-01"), RoleID = 1},
                new User { Username = "lib", FirstName = "gustav", LastName = "ganz", Email = "gustav@ganz.at", Password = "xxx", BirthDate = DateTime.Parse("2005-09-01"), CreationDate = DateTime.Parse("2005-09-01"), RoleID = 1},
                new User { Username = "pet" ,FirstName = "franz", LastName = "schön", Email = "franz@schön.at", Password = "xxx", BirthDate = DateTime.Parse("2005-09-01"), CreationDate = DateTime.Parse("2005-09-01"), RoleID = 1},
                new User { Username = "greg", FirstName = "andreas", LastName = "bauer", Email = "andreas@bauer.at", Password = "xxx", BirthDate = DateTime.Parse("2005-09-01"), CreationDate = DateTime.Parse("2005-09-01"), RoleID = 1},
            };
            users.ForEach(s => context.Users.Add(s));
            context.SaveChanges();

            var shirts = new List<Shirt>
            {
                new Shirt {UserID = 1, Name = "shirt1", Description= "User erstes Shirt"},
                new Shirt {UserID = 1, Name = "shirt2", Description= "User zweites Shirt"},
                new Shirt {UserID = 2, Name = "shirt3", Description= "User drittes Shirt"},
                new Shirt {UserID = 2, Name = "shirt4", Description= "User vierte Shirt"},
                new Shirt {UserID = 2, Name = "shirt5", Description= "User fünftes Shirt"},
            };
            shirts.ForEach(s => context.Shirts.Add(s));
            context.SaveChanges();

        }
    }
}