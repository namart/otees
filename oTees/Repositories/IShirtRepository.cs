﻿using oTees.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace oTees.Repositories
{
    public interface IShirtRepository
    {
        IEnumerable<Shirt> GetAll();
        Shirt Get(int id, bool uncached = false);
        Shirt Add(Shirt item, User user);
        void Remove(int id);
        bool Update(Shirt item);
    }
}