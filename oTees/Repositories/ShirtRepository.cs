﻿using oTees.DAL;
using oTees.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace oTees.Repositories
{
    public class ShirtRepository : IShirtRepository
    {

        private OteesDBContext context = new OteesDBContext();

        public IEnumerable<Models.Shirt> GetAll()
        {
            var shirts = context.Shirts;
            return shirts.ToList();
        }

        public Models.Shirt Get(int id, bool uncached = false)
        {

            if (uncached)
            {
                return context.Shirts.Find(id);
            }

            Models.Shirt cachedShirt = Caching.CacheController.getShirt(id);

            if (cachedShirt == null)
            {
                Models.Shirt newShirt = context.Shirts.Find(id);
                if (newShirt != null)
                {
                    Caching.CacheController.insertShirt(newShirt);
                    return newShirt;
                }
                else
                {
                    return null;
                }

            }
            else
            {
                return cachedShirt;
            }

        }

        public Models.Shirt Add(Models.Shirt item, User user)
        {
            User userDb = context.Users.Find(user.UserID);
            

            Shirt shirt = new Shirt();
            shirt.Name = item.Name;
            shirt.Description = item.Description;
            shirt.Images = item.Images;
            shirt.User = userDb;
            context.Shirts.Add(shirt);
            context.SaveChanges();
            return shirt;
        }

        public void Remove(int id)
        {
            Shirt shirt = this.Get(id, true);
            context.Shirts.Remove(shirt);
            context.SaveChanges();
        }

        public bool Update(Models.Shirt item)
        {
            var shirtInDb = this.Get(item.ShirtID, true);
            if (shirtInDb == null)
            {
                return false;
            }

            shirtInDb.Name = item.Name;
            shirtInDb.Description = item.Description;
            shirtInDb.Images = item.Images;

            context.Entry(shirtInDb).State = System.Data.EntityState.Modified;
            context.SaveChanges();

            return true;
        }
    }
}