﻿using oTees.DAL;
using oTees.Models;
using oTees.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace oTees.Repositories
{
    public class UserRepository : IUserRepository
    {
        private OteesDBContext context = new OteesDBContext();

        public IEnumerable<User> GetAll()
        {
            var users = context.Users;
            return users.ToList();
        }

        public User GetUserByEmail(string email)
        {
            List<User> userDB = context.Users.Where(x => x.Email.Equals(email)).ToList();
            if (userDB.Count() == 0) return null;
            return userDB.First();
        }

        public User GetUserByUsername(string username)
        {
            List<User> userDB = context.Users.Where(x => x.Username.Equals(username)).ToList();
            if (userDB.Count() == 0) return null;
            return userDB.First();
        }

        public User Get(int id)
        {
            return context.Users.Find(id);
        }

        public User Add(User item)
        {
            throw new NotImplementedException();
        }

        public void Remove(int id)
        {
            User user = context.Users.Find(id);
            context.Users.Remove(user);
            context.SaveChanges();
        }

        public bool Update(EditUserViewModel user)
        {
            var userInDb = this.Get(user.UserID);
            if (userInDb == null)
            {
                return false;
            }
            userInDb.FirstName = user.FirstName;
            userInDb.LastName = user.LastName;
            userInDb.Email = user.Email;
            userInDb.BirthDate = user.BirthDate;
            userInDb.RoleID = user.RoleID;
            context.Entry(userInDb).State = System.Data.EntityState.Modified;
            context.SaveChanges();

            return true;
        }
    }
}