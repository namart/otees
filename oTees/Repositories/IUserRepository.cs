﻿using oTees.Models;
using oTees.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace oTees.Repositories
{
    public interface IUserRepository
    {
        IEnumerable<User> GetAll();
        User GetUserByEmail(string email);
        User GetUserByUsername(String username);
        User Get(int id);
        User Add(User item);
        void Remove(int id);
        bool Update(EditUserViewModel item);
    }
}