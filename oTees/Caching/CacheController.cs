﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace oTees.Caching
{
    public static class CacheController
    {
        //private static Cache shirtCache = new Cache();

        public static Models.Shirt getShirt(int id)
        {
            lock (typeof(CacheController))
            {
                if (HttpContext.Current.Cache.Get("shirt_" + id.ToString()) != null)
                {
                    return (Models.Shirt)HttpContext.Current.Cache.Get("shirt_" + id.ToString());
                }
                return null;
            }
        }

        public static void insertShirt(Models.Shirt shirt)
        {
            lock(typeof(CacheController)) {
                HttpContext.Current.Cache.Insert("shirt_" + shirt.ShirtID.ToString(), shirt, null, DateTime.Now.AddSeconds(10), System.Web.Caching.Cache.NoSlidingExpiration);
            }
        }
    }
}