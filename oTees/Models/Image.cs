﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace oTees.Models
{
    public class Image
    {
        public int ImageID { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }

        public int ShirtID { get; set; }
        public virtual Shirt Shirt { get; set; }
    }
}