﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace oTees.Models
{
    public class User
    {
        public int UserID { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie ein Passwort an")]
        public string Password { get; set; }
        public string PasswordSalt { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie einen Benutzernamen an")]
        [DisplayName("Benutzername")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie einen Namen an")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie einen Namen an")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie eine Email an")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Bitte geben sie eine email an")]
        public string Email { get; set; }

        public DateTime CreationDate { get; set; }

        public String Activation { get; set; }

        public String RecoverKey { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie ihren Geburtstag an")]
        [DisplayName("Geburtstag")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime BirthDate { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie eine Rolle an")]
        public int RoleID { get; set; }
        public virtual Role Role { get; set; }

    }
}