﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace oTees.Models.ViewModels
{
    public class UserViewModel
    {

        [Required(ErrorMessage = "Bitte geben Sie einen Benutzernamen an")]
        [DisplayName("Benutzername")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie einen Namen an")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie einen Namen an")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie ihren Geburtstag an")]
        [DisplayName("Geburtstag")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime BirthDate { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie eine Email an")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Bitte geben sie eine email an")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "&quote{0}&quote muss mindestens {2} Zeichen lang sein.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Kennwort")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Kennwort bestätigen")]
        [Compare("Password", ErrorMessage = "Das Kennwort entspricht nicht dem Bestätigungskennwort.")]
        public string ConfirmPassword { get; set; }

        public int RoleID { get; set; }

    }

    public class EditUserViewModel
    {
        [Required(ErrorMessage = "Bitte geben Sie einen Benutzernamen an")]
        [DisplayName("Benutzername")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie einen Namen an")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie einen Namen an")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie ihren Geburtstag an")]
        [DisplayName("Geburtstag")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime BirthDate { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie eine Email an")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Bitte geben sie eine email an")]
        public string Email { get; set; }

        public int RoleID { get; set; }

        public int UserID { get; set; }

    }
}