﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace oTees.Models
{
    public class Size
    {
        public int SizeID { get; set; }
        public string Name { get; set; }
    }
}