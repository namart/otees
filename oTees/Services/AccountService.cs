﻿using oTees.DAL;
using oTees.Models;
using oTees.Models.ViewModels;
using oTees.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;

namespace oTees.Services
{
    public class AccountService
    {

        private OteesDBContext db = new OteesDBContext();


        /*
         * Account Login Methods
         */

        public Boolean Login(String Email, String Password)
        {

            var userDB = from u in db.Users where u.Email == Email select u;
            if (userDB.Count() == 0) return false;
            User user = userDB.First();
            String passwordHashed = CreatePasswordHash(Password, user.PasswordSalt);

            //For testing without encryption
            if (userDB.Count() > 0 && (user.Password == Password) && user.Activation == null)
            {
                return true;
            }

            return false;
        }


        /*
         * Account Create Methods
         */

        public User CreateUser(UserViewModel model, Boolean active = false)
        {

            User user = new User();
            user.Username = model.Username;
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.Password = model.Password;
            user.Email = model.Email;
            user.BirthDate = model.BirthDate;
            user.RoleID = model.RoleID;
            user.CreationDate = DateTime.Now;

            user.PasswordSalt = CreateSalt();
            //user.Password = CreatePasswordHash(user.Password, user.PasswordSalt);
            user.Password = user.Password;
            user.CreationDate = DateTime.Now;
            if (!active)
            {
                user.Activation = ToSHA1Hash(user.Email);
            }
            
            db.Users.Add(user);
            db.SaveChanges();

            return user;
        }

        private string CreateSalt()
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[32];
            rng.GetBytes(buff);

            return Convert.ToBase64String(buff);
        }

        private string CreatePasswordHash(string pwd, string salt)
        {
            string saltAndPwd = String.Concat(pwd, salt);
            string hashedPwd = ToSHA1Hash(saltAndPwd);
            return hashedPwd;
        }

        public bool ChangePassword(string Email, string OldPassword, string NewPassword)
        {
            var userDB = from u in db.Users where u.Email == Email select u;
            if (userDB.Count() == 0) return false;
            User user = userDB.First();
            if (user.Password == OldPassword)
            {
                user.Password = NewPassword;
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        private string ToSHA1Hash(string str)
        {
            using (System.Security.Cryptography.SHA1 sha = new System.Security.Cryptography.SHA1CryptoServiceProvider())
            {
                var s = new MemoryStream();
                var sw = new StreamWriter(s);
                sw.Write(str);
                sw.Flush();
                s.Seek(0, SeekOrigin.Begin);

                var bytes = sha.ComputeHash(s);
                var sb = new StringBuilder();
                foreach (var b in bytes)
                {
                    sb.AppendFormat("{0:X2}", b);
                }
                return sb.ToString();
            }
        }

        public Boolean Activate(string activationKey)
        {
            var userDB = from u in db.Users where u.Activation == activationKey select u;
            if (userDB.Count() == 0) return false;

            User user = userDB.First();
            user.Activation = null;
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();

            return true;
        }

        public User setRecoverKey(EmailViewModel Email)
        {
            var userDB = from u in db.Users where u.Email == Email.Email select u;
            if (userDB.Count() == 0) return null;

            User user = userDB.First();
            user.RecoverKey = ToSHA1Hash(user.Email);
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();

            return user;
        }

        public User ChangePassword(PasswordRecoverViewModel Password)
        {
            var userDB = from u in db.Users where u.RecoverKey == Password.RecoveryKey select u;
            if (userDB.Count() == 0) return null;

            User user = userDB.First();
            user.PasswordSalt = CreateSalt();
            //user.Password = CreatePasswordHash(Password.NewPassword, user.PasswordSalt);
            user.Password = Password.NewPassword;
            user.RecoverKey = null;
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();

            return user;
        }

    }
}