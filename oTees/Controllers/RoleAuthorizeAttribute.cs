﻿using oTees.Models;
using oTees.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace oTees.Controllers
{
    public class RoleAuthorizeAttribute : AuthorizeAttribute
    {
        IUserRepository userRepository = new UserRepository();

        public new String Roles;
        private bool FailedRolesAuth = false;

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
           this.FailedRolesAuth = false;

            if (httpContext == null)
                throw new ArgumentNullException("httpContext");

            String userName = httpContext.User.Identity.Name;

            if (!httpContext.User.Identity.IsAuthenticated)
                return false;

            User user = userRepository.GetUserByEmail(userName);

            // check if the right user is accessing the data.

            if (user.Role.Name!= Roles && user.Role.Name != "Admin")
            {
                FailedRolesAuth = true;
                return false;
            }

            return true;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            if (FailedRolesAuth)
            {
                filterContext.Result = new ViewResult { ViewName = "NotAuth" };
            }
        }
    }
}