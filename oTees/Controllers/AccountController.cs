﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using oTees.Filters;
using oTees.Models;
using oTees.Models.ViewModels;
using oTees.Services;
using oTees.Repositories;

namespace oTees.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {

        private AccountService acc = new AccountService();
        private IUserRepository userRepository = new UserRepository();

        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid && acc.Login(model.Email, model.Password))
            {
                FormsAuthentication.SetAuthCookie(model.Email, model.RememberMe);
                return RedirectToLocal(returnUrl);
            }

            // Wurde dieser Punkt erreicht, ist ein Fehler aufgetreten; Formular erneut anzeigen.
            ModelState.AddModelError("", "Der angegebene Benutzername oder das angegebene Kennwort ist ungültig.");
            return View(model);
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Register

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(UserViewModel model)
        {

            if (ModelState.IsValid)
            {

                if (userRepository.GetUserByEmail(model.Email) != null)
                {
                    ModelState.AddModelError("Email", "Email wird schon verwendet");
                }
                else if (userRepository.GetUserByUsername(model.Username) != null)
                {
                    ModelState.AddModelError("Username", "Benutzername wird schon verwendet");
                }
                else
                {
                    model.RoleID = 1;
                    User user = acc.CreateUser(model);

                    //return RedirectToAction("Index", "Home");
                    return RedirectToAction("Activate", new { id = user.Activation });
                }

            }

            // hier email aktivierung schicken

            return View(model);
        }

        //
        // GET: /Account/Manage

        public ActionResult Manage(ManageMessageId? message)
        {

            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Ihr Kennwort wurde geändert."
                : message == ManageMessageId.SetPasswordSuccess ? "Ihr Kennwort wurde festgelegt."
                : message == ManageMessageId.RemoveLoginSuccess ? "Die externe Anmeldung wurde entfernt."
                : "";

            ViewBag.ReturnUrl = Url.Action("Manage");

            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(PasswordViewModel model)
        {

            if (ModelState.IsValid)
            {
                // "ChangePassword" löst in bestimmten Fehlerszenarien eine Ausnahme, anstatt "false" zurückzugeben.
                bool changePasswordSucceeded;
                try
                {
                    changePasswordSucceeded = acc.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                }
                else
                {
                    ModelState.AddModelError("", "Das aktuelle Kennwort ist nicht korrekt, oder das Kennwort ist ungültig.");
                }
            }
           

            // Wurde dieser Punkt erreicht, ist ein Fehler aufgetreten; Formular erneut anzeigen.
            return View(model);
        }

        //
        // GET: /Account/Activate/ActivationKey

        [AllowAnonymous]
        public ActionResult Activate(string id)
        {
            // Check for false activationkey
            acc.Activate(id);
            return View();
        }

        //
        // GET: /Account/PasswordForgot

        [AllowAnonymous]
        public ActionResult PasswordForgot(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/PasswordForgot

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult PasswordForgot(EmailViewModel model, string returnUrl)
        {

            if (ModelState.IsValid)
            {
                User user = acc.setRecoverKey(model);

                if (user == null)
                {
                    ModelState.AddModelError("Email", "Email does not exist");
                }
                else
                {
                    return RedirectToAction("PasswordRecover", new { id = user.RecoverKey });
                }

            }

            return View(model);
        }

        //
        // GET: /Account/PasswordForgot/RecoverKey

        [AllowAnonymous]
        public ActionResult PasswordRecover(String id)
        {
            PasswordRecoverViewModel model = new PasswordRecoverViewModel();
            model.RecoveryKey = id;
            return View(model);
        }

        //
        // POST: /Account/PasswordRecover

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult PasswordRecover(PasswordRecoverViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                User user = acc.ChangePassword(model);

                if (user == null)
                {
                    ModelState.AddModelError("RecoveryKey", "Key existiert nicht");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }

            }

            return View(model);
        }

        //
        // GET: /Account/CheckUsername/email

        [AllowAnonymous]
        public ActionResult CheckUsername(String username)
        {
            User user = userRepository.GetUserByUsername(username);
            if (user == null)
            {
                return Json("false", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("true", JsonRequestBehavior.AllowGet);
            }
            

            
        }

        #region Hilfsprogramme
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }
        #endregion
    }
}
