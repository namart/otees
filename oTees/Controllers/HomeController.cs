﻿using oTees.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace oTees.Controllers
{
    public class HomeController : Controller
    {

        private IShirtRepository shirtRepo = new ShirtRepository();

        public ActionResult Index()
        {
            var shirts = shirtRepo.GetAll();
            return View(shirts);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Ihre App-Beschreibungsseite.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Ihre Kontaktseite.";

            return View();
        }
    }
}
