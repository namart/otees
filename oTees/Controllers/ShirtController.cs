﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using oTees.Models;
using oTees.DAL;
using oTees.Repositories;
using oTees.Services;

namespace oTees.Controllers
{
    public class ShirtController : Controller
    {
        private IShirtRepository shirtRepo = new ShirtRepository();
        private IUserRepository userRepo = new UserRepository();
        private OteesDBContext db = new OteesDBContext();

        //
        // GET: /Shirt/

        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Shirt/Details/5

        public ActionResult Details(int id = 0)
        {
            Shirt shirt = shirtRepo.Get(id);
            if (shirt == null)
            {
                return HttpNotFound();
            }
            return View(shirt);
        }

        //
        // GET: /Shirt/Create

        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Shirt/Create

        [HttpPost]
        [Authorize]
        public ActionResult Create(Shirt shirt)
        {
            String userName = User.Identity.Name;

            User user = userRepo.GetUserByEmail(userName);

            if (ModelState.IsValid)
            {
                shirtRepo.Add(shirt, user);
                return RedirectToAction("Index");
            }

            return View(shirt);
        }

        //
        // GET: /Shirt/Edit/5

        [Authorize]
        public ActionResult Edit(int id = 0)
        {
            Shirt shirt = shirtRepo.Get(id);
            if (shirt == null)
            {
                return HttpNotFound();
            }
            return View(shirt);
        }

        //
        // POST: /Shirt/Edit/5

        [HttpPost]
        [Authorize]
        public ActionResult Edit(Shirt shirt)
        {
            if (ModelState.IsValid)
            {
                shirtRepo.Update(shirt);
                return RedirectToAction("Index");
            }
            return View(shirt);
        }

        //
        // GET: /Shirt/Delete/5

        [RoleAuthorize(Roles = "Admin")]
        public ActionResult Delete(int id = 0)
        {
            Shirt shirt = shirtRepo.Get(id);
            if (shirt == null)
            {
                return HttpNotFound();
            }
            return View(shirt);
        }

        //
        // POST: /Shirt/Delete/5

        [RoleAuthorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            shirtRepo.Remove(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}