﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using oTees.Models;
using oTees.DAL;
using oTees.Repositories;
using oTees.Services;
using oTees.Models.ViewModels;

namespace oTees.Controllers
{
    [RoleAuthorize(Roles = "Admin")]
    public class UserController : Controller
    {
        private OteesDBContext db = new OteesDBContext();
        private IUserRepository userRepository = new UserRepository();
        private AccountService acc = new AccountService();

        //
        // GET: /User/

        public ActionResult Index()
        {
            var users = userRepository.GetAll();
            return View(users);
        }

        //
        // GET: /User/Details/5

        public ActionResult Details(int id = 0)
        {
            User user = userRepository.Get(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //
        // GET: /User/Create

        public ActionResult Create()
        {
            ViewBag.RoleID = new SelectList(db.Roles, "RoleID", "Name");
            return View();
        }

        //
        // POST: /User/Create

        [HttpPost]
        public ActionResult Create(UserViewModel model)
        {
            if (ModelState.IsValid)
            {

                if (userRepository.GetUserByEmail(model.Email) != null)
                {
                    ModelState.AddModelError("Email", "Email wird schon verwendet");
                }
                else
                {
                    acc.CreateUser(model);
                    return RedirectToAction("Index");
                }

            }


            ViewBag.RoleID = new SelectList(db.Roles, "RoleID", "Name", model.RoleID);
            return View(model);
        }

        //
        // GET: /User/Edit/5

        public ActionResult Edit(int id = 0)
        {
            User user = userRepository.Get(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.RoleID = new SelectList(db.Roles, "RoleID", "Name", user.RoleID);
            return View(user);
        }

        //
        // POST: /User/Edit/5

        [HttpPost]
        public ActionResult Edit(EditUserViewModel user)
        {
            if (ModelState.IsValid)
            {
                userRepository.Update(user);
                return RedirectToAction("Index");
            }
            ViewBag.RoleID = new SelectList(db.Roles, "RoleID", "Name", user.RoleID);
            return View(user);
        }

        //
        // GET: /User/Delete/5

        public ActionResult Delete(int id = 0)
        {
            User user = userRepository.Get(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //
        // POST: /User/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            userRepository.Remove(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}